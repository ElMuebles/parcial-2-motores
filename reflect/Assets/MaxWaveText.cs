﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaxWaveText : MonoBehaviour
{
    public Text waverecordTXT;

    void Start()
    {
        waverecordTXT.text = "WAVE RECORD: " + PlayerPrefs.GetInt("HighestWave");
    }

}
