﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathPlane : MonoBehaviour
{
    // Start is called before the first frame update
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") == true)
        {
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene("YouDied");
        }

        if (collision.gameObject.CompareTag("Enemy") == true)
        {
            Destroy(collision.gameObject);
        }


    }
}
