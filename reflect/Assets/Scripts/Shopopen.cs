﻿using UnityEngine;

public class Shopopen : MonoBehaviour
{

    public GameObject player;
    public GameObject shopkeeper;

    public Camera ShopCamera;
    public Camera PlayerCamera;
    public static bool shopOpening;
    public float TargetDistance;
    private float CurrentDistance;
    Animator anim;

    private void Start()
    {
        TargetDistance = 20.4f;
        anim = GetComponent<Animator>();
        PlayerCamera.enabled = true;
        ShopCamera.enabled = false;
        shopOpening = false;
    }

    private void Update()
    {
        CurrentDistance = Vector3.Distance(player.transform.position, shopkeeper.transform.position);

        if (CurrentDistance <= TargetDistance)
        {
            anim.SetBool("NearPlayer", true);
        }
        else { anim.SetBool("NearPlayer", false);
        }

    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("PlayerDMG") == true && swordHit.HitboxSwordOn == true)
        {
            Debug.LogError("PuedeAtacar es false");

            swordHit.PuedeAtacar = false;
            ShopCamera.enabled = true;
            PlayerCamera.enabled = false;
            shopOpening = true;
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
