﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySlimeScript : MonoBehaviour
{
    Animator anim;

    public static bool tutorialSlimeDead = false;

    private bool activatedOnce;
    private bool isStunned;
    private bool playerShielded;
    private bool PlayerInvulnerable = false;
    private bool SwordRemovesStun = false;

    private bool newAttackReady = true;
    public int velocidad;

    public static Rigidbody jugador;

    public static int DamagePower = 10;

    public Rigidbody slime;
    public Camera SlimeEyes;
    public float distance;
    public Vector3 knockback;

    private int slimeHP = 30;
    private bool isAlive = true;

    public void Start()
    {
        velocidad = Random.Range(1, 5);
        anim = GetComponent<Animator>();
        jugador = ControlJugador.rb;
    }

    private void Update()
    {
        transform.LookAt(jugador.transform);
        transform.Translate(velocidad * Vector3.forward * Time.deltaTime);

        if (Vector3.Distance(this.transform.position, jugador.transform.position) <= 4 && newAttackReady == true && isAlive == true && isStunned == false)
        {
            newAttackReady = false;
            anim.SetTrigger("DoDamage");
            StartCoroutine(nameof(FaseAtaque));
        }

        if (velocidad > 0)
        {
            anim.SetBool("IsWalking", true);

        }
    }

    public IEnumerator Death()
    {
        yield return new WaitForSeconds(3);
        Destroy(gameObject);
        ShopManagerScript.SpCoins += 10 + ButtonInfo.currentMoneyDrop;
        Debug.Log(ShopManagerScript.SpCoins);
    }

    public IEnumerator FaseAtaque()
    {
        if (SwordRemovesStun == true)
        {
            SwordRemovesStun = false;
            velocidad = Random.Range(1, 5);
        } 

        if (isAlive == true) { 
             if (isStunned == true)
             {
                 isStunned = false;
                velocidad = 0;
               yield return new WaitForSeconds(3f);
                Debug.Log("TerminoStun");
                anim.SetBool("IsStunned", false);
                playerShielded = false;
               velocidad = Random.Range(1, 5);

             }

            if (PlayerInvulnerable == true)
            {
            Debug.Log("termino invuln.");
            yield return new WaitForSeconds(1f);
            PlayerInvulnerable = false;
            }

             yield return new WaitForSeconds(2f);
             newAttackReady = true;
             Debug.Log("empezo ataque");

        }
    }

    [SerializeField] private float knockbackStrength = 10;
    private void OnTriggerEnter(Collider collision)
    {
        //Slime Recieves Damage

        if (collision.gameObject.CompareTag("PlayerDMG") == true && swordHit.HitboxSwordOn == true)
        {
            slimeHP -= ButtonInfo.currentPlayerDamage;
            Debug.Log(slimeHP);

            if (slimeHP > 0)
            {
                SwordRemovesStun = true;
                swordHit.HitboxSwordOn = false;
                Vector3 direction = transform.position - jugador.transform.position;
                slime.AddForce(direction.normalized * knockbackStrength * -1, ForceMode.Impulse);
                anim.SetTrigger("DamageRecieve");
                FindObjectOfType<AudioManager>().Play("SwordClash");
                StartCoroutine("FaseAtaque");                
            }

            if (slimeHP <= 0)
            {
                swordHit.HitboxSwordOn = false;

                tutorialSlimeDead = true; //Script Especifico para el Primer slime, si se repite no pasa nada
                isAlive = false;
                velocidad = 0;
                anim.SetBool("DieBool", true);
                StartCoroutine(nameof(Death));
                FindObjectOfType<AudioManager>().Play("SwordClash");

            }

        }
    }

    private void OnTriggerStay(Collider collision) { 
        //Slime Attacks You

        if (newAttackReady == false && playerShielded == false && PlayerInvulnerable == false) //If slime esta atacando
        {

           if (collision.gameObject.CompareTag("Shield") == true && Reflect.estaDefendiendo == true) // Si choca con tu escudo y estas defendiendo
           {
                //estetica
                anim.SetBool("IsStunned", true);
                FindObjectOfType<AudioManager>().Play("ShieldHit");

                //codigo
                playerShielded = true; //Se Defendiendo
                isStunned = true; //El Enemigo esta stuneado
                StartCoroutine(nameof(FaseAtaque));
           }

           else if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("Shield"))
           {
                FindObjectOfType<AudioManager>().Play("PlayerHurt");
                Vector3 direction = jugador.transform.position - transform.position;
                direction.y = 0;
                jugador.AddForce(direction.normalized * knockbackStrength, ForceMode.Impulse);

                ControlJugador.HP -= DamagePower;
                Debug.LogWarning("Perdiste vida");

                PlayerInvulnerable = true;
           }
        }
    }
}

