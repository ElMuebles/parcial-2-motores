﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
public class AlmacenarDatos : MonoBehaviour
{


    public AlmacenarDatos instancia;
    public DataPersistencia data;
    public string savedata = "save.dat";
    public static int puntuacion;

    void Update()
    {

        if ((puntuacion < WaveSpawner.HighestWave))
        {
            data.RecordWaves = WaveSpawner.HighestWave;
            WaveSpawner.HighestWave = 0;
            SaveData();
        }

        if (ControlJugador.HP <= 0)
        {
            LoadData();
        }
    }

    private void Awake()
    {
    }

    public void SaveData()
    {
        string filePath = Application.persistentDataPath + "/" + savedata;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Create(filePath);
        bf.Serialize(file, data);
        file.Close();
        Debug.Log("Datos guardados");


    }

    public void LoadData()
    {
        string filePath = Application.persistentDataPath + "/" + savedata;
        BinaryFormatter bf = new BinaryFormatter();
        if (File.Exists(filePath))
        {
            FileStream file = File.Open(filePath, FileMode.Open);
            DataPersistencia cargado = (DataPersistencia)bf.Deserialize(file);
            data = cargado;
            file.Close();
            Debug.Log("Datos cargados");


                puntuacion = data.RecordWaves;
            
        }

    }

    [System.Serializable]
    public class DataPersistencia
    {
        public int RecordWaves;

        public DataPersistencia()
        {
            puntuacion = RecordWaves;
            
        }
    }

}
