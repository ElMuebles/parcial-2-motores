﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathPlane : MonoBehaviour
{
    // Start is called before the first frame update


    // Update is called once per frame
    void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.CompareTag("Player") || collision.gameObject.CompareTag("Shield"))
        {
            ControlJugador.HP = 0;
        }

        if (collision.gameObject.CompareTag("Enemy"))
        {
            Destroy(collision);
        }
    }
}
