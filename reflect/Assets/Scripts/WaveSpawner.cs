﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaveSpawner : MonoBehaviour
{
    public enum SpawnState { SPAWNING, WAITING, COUNTING }

    [System.Serializable]
    public class Wave
    {
        public string name;
        public Transform enemy;
        public int enemyCount;
        public float rate;
    }

    public Wave[] waves;
    private int nextWave = 0;

    public Text waveTXT;
    private int waveAmount = 0;
    public static int HighestWave;

    public Transform[] spawnPoints;

    public float TiempoEntreWaves = 5f;
    private float waveCountdown;

    private float searchCountdown = 1f;

    public SpawnState state = SpawnState.COUNTING;

    void Start()
    {
        if (spawnPoints.Length == 0)
        {
            Debug.LogError("No SpawnPoints referenced.");
        }

        waveCountdown = TiempoEntreWaves;
    }

    void Update()
    {
        if (state == SpawnState.WAITING)
        {
            if (EnemyIsAlive() == false)
            {
                WaveCompleted();
                return;
            }
            else
            {
                return;
            }
        }

        if (waveCountdown <= 0)
        {
            if (state != SpawnState.SPAWNING)
            {
                StartCoroutine(SpawnWave(waves[nextWave]));
            }

        }
        else
        {
            waveCountdown -= Time.deltaTime;
        }

    }

    void WaveCompleted()
    {
        waveTXT.text = "WAVE " + waveAmount + " COMPLETED!! (GO BUY ITEMS)";
        

        Debug.Log("Ronda Completada");
        state = SpawnState.COUNTING;
        waveCountdown = TiempoEntreWaves;

        if (nextWave + 1 > waves.Length - 1)
        {
            nextWave = 0;
            EnemySlimeScript.DamagePower += 10;
            Debug.Log("LISTO, SE TERMINARON LAS WAVES. LOOP");
        }

        else
        {
            nextWave++;
        }

    }

    bool EnemyIsAlive()
    {
        
        searchCountdown -= Time.deltaTime;
        if (searchCountdown <= 0f)
        {
            searchCountdown = 1f;
            if (GameObject.FindGameObjectWithTag("Enemy") == null)
            {
                Debug.LogWarning("No se encontraron enemigos");
                return false;
            }
        }
            return true;
        
    }

    IEnumerator SpawnWave(Wave _wave)
    {
        if (EnemySlimeScript.tutorialSlimeDead == true) {
            waveAmount++;

            if (HighestWave < waveAmount)
            {
                HighestWave = waveAmount;
            }

            waveTXT.text = "WAVE " + waveAmount + " STARTING...";
            Debug.Log("tutorialdead");
            state = SpawnState.SPAWNING;

            for (int i = 0; i < _wave.enemyCount; i++)
            {
              SpawnEnemy(_wave.enemy);
              yield return new WaitForSeconds(1f / _wave.rate);
            }

            state = SpawnState.WAITING;
            waveTXT.text = "WAVE " + waveAmount;
            yield break;
        }
    }

    void SpawnEnemy(Transform _enemy)
    {
        Debug.Log("SpawnEnemy: " + _enemy.name);

        Transform _sp = spawnPoints[Random.Range(0, spawnPoints.Length)];
        Instantiate(_enemy, _sp.position, _sp.rotation);

    }
}
