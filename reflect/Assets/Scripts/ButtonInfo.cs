﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ButtonInfo : MonoBehaviour
{

    public static int currentPlayerDamage = 10;
    public static int currentMoneyDrop = 0; //Sin contar drop de base
    public static float currentAttackSpeed = 0.5f;
    public static float currentPlayerSpeed = 10.0f;

    public static bool CurrentHPRefresh;

    public Camera PlayerCamera;
    public Camera ShopCamera;

    public int ButtonPress;
    public int ItemID;
    public Text PriceText;
    public Text QuantityText;
    public GameObject ShopManager;

    void Update()
    {
        PriceText.text = "Price: " + ShopManager.GetComponent<ShopManagerScript>().shopItems[2, ItemID].ToString();
        QuantityText.text = ShopManager.GetComponent<ShopManagerScript>().shopItems[3, ItemID].ToString();

        if (ButtonPress == 1)
        {
            FindObjectOfType<AudioManager>().Play("Cachin");
            ButtonPress = 0;
            currentPlayerSpeed += 1.0f;
        }

        if (ButtonPress == 2)
        {
            FindObjectOfType<AudioManager>().Play("Cachin");
            ButtonPress = 0;
            currentMoneyDrop += 5;
        }

        if (ButtonPress == 3)
        {
            FindObjectOfType<AudioManager>().Play("Cachin");
            ButtonPress = 0;
            currentAttackSpeed -= 0.2f;
        }

        if (ButtonPress == 4)
        {
            FindObjectOfType<AudioManager>().Play("Cachin");
            ButtonPress = 0;
            currentPlayerDamage += 10;
        }

        if (ButtonPress == 4)
        {
            FindObjectOfType<AudioManager>().Play("Cachin");
            ButtonPress = 0;
            currentPlayerDamage += 10;
        }

        if (ButtonPress == 5)
        {
            FindObjectOfType<AudioManager>().Play("Cachin");
            ButtonPress = 0;
            ControlJugador.HP += 20;
            CurrentHPRefresh = true;
            Debug.Log(ControlJugador.HP);
        }

        if (ButtonPress == 6)
        {
            
            ButtonPress = 0;
            Cursor.lockState = CursorLockMode.Locked;
            Shopopen.shopOpening = false;
            swordHit.PuedeAtacar = true;
            PlayerCamera.enabled = true;
            ShopCamera.enabled = false;
        }
    }
}
