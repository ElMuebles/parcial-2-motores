﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ShopManagerScript : MonoBehaviour
{


    public int[,] shopItems = new int[7, 7];
    public static float SpCoins = 100;
    public Text CoinsTXT;


    void Start()
    {
       CoinsTXT.text = "SP: " + SpCoins.ToString();

        //IDs
        shopItems[1, 1] = 1;
        shopItems[1, 2] = 2;
        shopItems[1, 3] = 3;
        shopItems[1, 4] = 4;
        shopItems[1, 5] = 5;
        shopItems[1, 6] = 6;

        //Precios
        shopItems[2, 1] = 10;
        shopItems[2, 2] = 20;
        shopItems[2, 3] = 30;
        shopItems[2, 4] = 40;
        shopItems[2, 6] = 0;

        //Quantity

        shopItems[3, 1] = 0;
        shopItems[3, 2] = 0;
        shopItems[3, 3] = 0;
        shopItems[3, 4] = 0;
        shopItems[2, 5] = 0;
        shopItems[2, 6] = 0;

    }

    private void Update()
    {
        CoinsTXT.text = "SP: " + SpCoins.ToString();
        shopItems[2, 5] = 50;
    }

    public void Buy()
    {
        GameObject ButtonRef = GameObject.FindGameObjectWithTag("Event").GetComponent<EventSystem>().currentSelectedGameObject;

        if (SpCoins >= shopItems[2, ButtonRef.GetComponent<ButtonInfo>().ItemID])
        {

            SpCoins -= shopItems[2, ButtonRef.GetComponent<ButtonInfo>().ItemID];
            shopItems[3, ButtonRef.GetComponent<ButtonInfo>().ItemID]++;

            //Updates
            CoinsTXT.text = "SP: " + SpCoins.ToString();
            ButtonRef.GetComponent<ButtonInfo>().QuantityText.text = shopItems[3, ButtonRef.GetComponent<ButtonInfo>().ItemID].ToString();
            ButtonRef.GetComponent<ButtonInfo>().ButtonPress = ButtonRef.GetComponent<ButtonInfo>().ItemID;
        }
    }

}
