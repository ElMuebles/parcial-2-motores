﻿
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGame : MonoBehaviour
{

    public void PlayGame()
    {
        SceneManager.LoadScene("Openworld");
        ButtonInfo.currentPlayerDamage = 10;
        ButtonInfo.currentMoneyDrop = 0; //Sin contar drop de base
        ButtonInfo.currentAttackSpeed = 0.5f;
        ButtonInfo.currentPlayerSpeed = 10.0f;

        EnemySlimeScript.tutorialSlimeDead = false;
        ShopManagerScript.SpCoins = 100;

        ControlJugador.HP = 100;
        ButtonInfo.CurrentHPRefresh = true;
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
