﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MaxWaveReached
{
    public int WaveRecord;

    public MaxWaveReached(WaveSpawner wave)
    {
        WaveRecord = wave.HighestWave;

    }
}
