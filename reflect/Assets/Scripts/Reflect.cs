﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Reflect : MonoBehaviour
{
    public Camera MainCamera;
    Animator anim;
    private bool PuedeDefender=true;
    public static bool estaDefendiendo = false;
    public float proteccionMax = 0.30f;
    public float DisponibilidadEscudo = 2.50f; // Despues de Proteccion Max


    private void Start()
    {
        anim = GetComponent<Animator>();

    }

    private void Update()
    {

        if (PuedeDefender == true) {
            if (Input.GetMouseButtonDown(1))
            {

                PuedeDefender = false;
                anim.SetTrigger("Defend");
                
                StartCoroutine(nameof(MantenerEscudo));
            }
        }
    }

    private IEnumerator MantenerEscudo()
    {
        estaDefendiendo = true;
        yield return new WaitForSeconds(proteccionMax);
        estaDefendiendo = false;
        yield return new WaitForSeconds(DisponibilidadEscudo);
        PuedeDefender = true;
    }


    void OnTriggerEnter(Collider collision)
    {

        if (collision.gameObject.CompareTag("EnemyShot") == true)
        {
            if (estaDefendiendo == true)
            {
                Destroy(collision.gameObject);
            }
        }
    }
}

