﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{

    public Slider slider;

    public void SetMaxHealth()
    {
        slider.maxValue = ControlJugador.HP;
        slider.value = ControlJugador.HP;
    }

    public void Update()
    {
        slider.value = ControlJugador.HP;
        if (ButtonInfo.CurrentHPRefresh == true)
        {
            SetMaxHealth();
            ButtonInfo.CurrentHPRefresh = false;
        }

    }

    public void SetHealth()
    {

        slider.value = ControlJugador.HP; 
    }

}
