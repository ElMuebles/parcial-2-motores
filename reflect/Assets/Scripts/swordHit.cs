﻿using System.Collections;
using UnityEngine;

public class swordHit : MonoBehaviour
{
    Animator anim;

    public GameObject shield;
    public GameObject particles;
    public Camera MainCamera;
    public static bool PuedeAtacar;
    public static bool HitboxSwordOn = false;

    private void Start()
    {
        particles.SetActive(false);
        anim = GetComponent<Animator>();
    }

    private IEnumerator HabilitarEspada()
    {
        HitboxSwordOn = true;
        particles.SetActive(true);
        yield return new WaitForSeconds(0.3f);
        HitboxSwordOn = false;
        particles.SetActive(false);
        yield return new WaitForSeconds(ButtonInfo.currentAttackSpeed);
        if (Shopopen.shopOpening == false) { 
        PuedeAtacar = true;
        }
    }

    void Update()
    {
        if (PuedeAtacar == true)
        {

            if (Input.GetMouseButtonDown(0))
            {
                FindObjectOfType<AudioManager>().Play("SwordWoosh");
                PuedeAtacar = false;
                anim.SetTrigger("Attack");
                StartCoroutine(nameof(HabilitarEspada));
                
            }
        }



    }


}
