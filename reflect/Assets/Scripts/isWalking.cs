﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class isWalking : MonoBehaviour
{
    Animator anim;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }
    void Update()
    {
        if (ControlJugador.isMoving == true)
        {

            anim.SetBool("isWalking", true);
        } else anim.SetBool("isWalking", false);

    }
}
