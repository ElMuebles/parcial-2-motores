﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ControlJugador : MonoBehaviour
{
    public static int HP = 100;
    public bool PuedeSaltar;
    public Camera MainCamera;
    public static bool isMoving = false;

    public Vector3 jump;
    public float FuerzaDeSalto = 1.0f;
   public static Rigidbody rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        swordHit.PuedeAtacar = true;
    }

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        jump = new Vector3(0.0f, 1.0f, 0.0f);
        rb = GetComponent<Rigidbody>();
    }

    void OnCollisionStay()
    {
        PuedeSaltar = true;
    }

    void Update()
    {

        //Mov Jugador

        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * ButtonInfo.currentPlayerSpeed;
        float movimientoCostados = Input.GetAxis("Horizontal") * ButtonInfo.currentPlayerSpeed;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        //Saltar
        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (movimientoCostados != 0 || movimientoAdelanteAtras != 0)
        {
            isMoving = true;
        }
        else
        {
            isMoving = false;
        }

        if (Input.GetKeyDown(KeyCode.Space) && PuedeSaltar)
        {
            rb.AddForce(jump * FuerzaDeSalto, ForceMode.Impulse);
            PuedeSaltar = false;
        }

        if (HP <= 0)
        {
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene("YouDied");
        }
    }
}

