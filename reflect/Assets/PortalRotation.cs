﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalRotation : MonoBehaviour
{

    void Update()
    {
        transform.LookAt(EnemySlimeScript.jugador.transform);

        var x = Input.GetAxis("Vertical") * Time.deltaTime;

        transform.Rotate(x, 90, 90); 
    }
}
